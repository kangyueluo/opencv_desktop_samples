package Ch9;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.photo.Photo;

import java.util.ArrayList;
import java.util.List;

public class OpenCV_Denoise3 {

    public static void main(String args[]){
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        Mat im = Imgcodecs.imread("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\lena.jpg",Imgcodecs.CV_LOAD_IMAGE_COLOR);
        Mat im1 = Imgcodecs.imread("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\lena1.jpg",Imgcodecs.CV_LOAD_IMAGE_COLOR);
        Mat im2 = Imgcodecs.imread("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\lena2.jpg", Imgcodecs.CV_LOAD_IMAGE_COLOR);
        List<Mat> srcList=new ArrayList<Mat>();
        Mat dst=new Mat();
        srcList.add(im1);
        srcList.add(im2);
        srcList.add(im);
        Photo.fastNlMeansDenoisingMulti(srcList, dst, 2, 1);
        Imgcodecs.imwrite("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\fastNlMeansDenoisingMulti_lena.jpg", dst);
    }

}
