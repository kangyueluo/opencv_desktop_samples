package Ch9;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.photo.Photo;

import javax.swing.*;
import java.awt.*;

public class OpenCV_Decolor {
    static {System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}
    private JFrame frmjavaSwing;
    double alpha =5;
    double beta =50;

    /**
     *  Launch the application.
     */

    public static void main(String[] args){
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        Mat im = Imgcodecs.imread("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\lena.jpg");
        Mat color_boost=new Mat();
        Mat grayscale=new Mat();
        Photo.decolor(im, grayscale, color_boost);
        Imgcodecs.imwrite("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\lena_boost.jpg", color_boost);
        Imgcodecs.imwrite("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\lena_boost_gray.jpg", grayscale);
    }
}
