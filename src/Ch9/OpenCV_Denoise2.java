package Ch9;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.photo.Photo;

public class OpenCV_Denoise2 {

    public static void main(String args[]){
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        Mat im = Imgcodecs.imread("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\lena.jpg");
        Mat dst=new Mat();
        Photo.fastNlMeansDenoisingColored(im, dst);
        Imgcodecs.imwrite("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\fastNlMeansDenoisingColored_lena.jpg", dst);
    }
}
