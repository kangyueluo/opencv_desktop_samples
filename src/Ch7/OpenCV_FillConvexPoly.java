package Ch7;

import java.awt.EventQueue;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

public class OpenCV_FillConvexPoly {
    static {System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}

    private JFrame frmjavaSwing;

    /**
        * Launch the application.
        */
    public static void main(String[] argv){
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try{
                    OpenCV_FillConvexPoly window = new OpenCV_FillConvexPoly();
                    window.frmjavaSwing.setVisible(true);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    /**
        * Create the application.
        */
    public OpenCV_FillConvexPoly(){
        init();
    }

    /**
        * Init the contens of the frame.
        */
    private void init(){
        Mat source =  paint();
        BufferedImage image=matToBufferedImage(source);

        frmjavaSwing = new JFrame();
        frmjavaSwing.setTitle("opencv ¶ñ¸É¥Y¦hÃä§Î½m²ß");
        frmjavaSwing.setBounds(100, 100, 300, 300);
        frmjavaSwing.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmjavaSwing.getContentPane().setLayout(null);

        final JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setBounds(5, 5, image.getHeight()+10, image.getWidth()+10);
        lblNewLabel.setIcon(new ImageIcon(image));
        frmjavaSwing.getContentPane().add(lblNewLabel);
    }
    public Mat paint(){

        Mat source = new Mat(250,250, CvType.CV_8UC3, new Scalar(255,255,255));
        MatOfPoint mop=new MatOfPoint();
        List<Point> allPoint=new ArrayList<Point>();
        allPoint.add(new Point(38,21));
        allPoint.add(new Point(13,128));
        allPoint.add(new Point(13,139));
        allPoint.add(new Point(155,240));
        allPoint.add(new Point(158,167));
        allPoint.add(new Point(200,219));
        allPoint.add(new Point(243,143));
        allPoint.add(new Point(154,26));
        mop.fromList(allPoint);
        Imgproc.fillConvexPoly(source, mop, new Scalar(255,0,0));
        return source;
    }
    public BufferedImage matToBufferedImage(Mat matrix) {
        int cols = matrix.cols();
        int rows = matrix.rows();
        int elemSize = (int)matrix.elemSize();
        byte[] data = new byte[cols * rows * elemSize];
        int type;
        matrix.get(0, 0, data);
        switch (matrix.channels()) {
            case 1:
                type = BufferedImage.TYPE_BYTE_GRAY;
                break;
            case 3:
                type = BufferedImage.TYPE_3BYTE_BGR;
                // bgr to rgb
                byte b;
                for(int i=0; i<data.length; i=i+3) {
                    b = data[i];
                    data[i] = data[i+2];
                    data[i+2] = b;
                }
                break;
            default:
                return null;
        }
        BufferedImage image2 = new BufferedImage(cols, rows, type);
        image2.getRaster().setDataElements(0, 0, cols, rows, data);
        return image2;
    }
}
