package Ch7;

import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class OpenCV_FloodFill {

    public static void main(String[] args){
        try{
            System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
            Mat src = Imgcodecs.imread("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\test-floodFill.jpg");
            System.out.println("Imgproc.FLOODFILL_FIXED_RANGE="+Imgproc.FLOODFILL_FIXED_RANGE);
            System.out.println("Imgproc.FLOODFILL_MASK_ONLY="+ Imgproc.FLOODFILL_MASK_ONLY);
            Point seedPoint = new Point(85,89);
            Mat mask = new Mat();
            Rect rect = new Rect();
            Imgproc.floodFill(src, mask, seedPoint, new Scalar(0,130,120), rect, new Scalar(20,20,20),new Scalar(20,20,20), 4);
            Imgcodecs.imwrite("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\test-floodFill_do.jpg", src);


        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
