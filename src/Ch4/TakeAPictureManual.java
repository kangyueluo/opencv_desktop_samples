package Ch4;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.videoio.VideoCapture;

public class TakeAPictureManual {

	static {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}
	
	public static void main(String[] args) {
		VideoCapture camera = new VideoCapture();
		camera.open(0);
		JFrame frame1 = new JFrame("Show image");
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame1.setTitle("�q webcam Ū���v���� Java swing ����");
		frame1.setSize(600,480);
		frame1.setBounds(0,0,frame1.getWidth(),frame1.getHeight());
		Panel panel1 = new Panel();
		frame1.setContentPane(panel1);
		frame1.setVisible(true);
		
		JButton btnCapture = new JButton("�� ��");
		btnCapture.setEnabled(false);
		btnCapture.setBounds(680,97,87,23);
		frame1.getContentPane().add(btnCapture);
		
		if(camera.isOpened()) {
			final Mat webcam_preview = new Mat();
			camera.read(webcam_preview);
			frame1.setSize(webcam_preview.width()+50,webcam_preview.height()+70);
			btnCapture.setEnabled(true);
			btnCapture.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					Imgcodecs.imwrite("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\manual_take_webcam_frame.jpg",webcam_preview);
					System.out.println("���Ĳ�o���");
				}
			});
			
			while (true) {
				
				try {
					camera.read(webcam_preview);
					Thread.sleep(500);
					camera.read(webcam_preview);
					Thread.sleep(500);
					camera.read(webcam_preview);
					Thread.sleep(500);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			
				System.out.println("�ϥ���v�����");
				panel1.setimagewithMat(webcam_preview);
				frame1.repaint();
			}
			
		}else {
			System.out.println("Error");
		}
		
		
	}

}
