package Ch4;

import javax.swing.JFrame;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;

public class OpenCV_webcam3 {


	
	static {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}
	
	public static void main(String[] args) {
	
		VideoCapture camera = null;
		camera =  initWebcam();
		JFrame frame1 = new JFrame("Show image");
		frame1.setTitle("�q webcam Ū���v���� Java swing ����");
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame1.setSize(640, 480);
		frame1.setBounds(0,0,frame1.getWidth(),frame1.getHeight());
		Panel panel1 = new Panel();
		frame1.setContentPane(panel1);
		frame1.setVisible(true);
		
		if(camera.isOpened()) {
			Mat webcam_frame = new Mat();
			camera.read(webcam_frame);
			frame1.setSize(webcam_frame.width(),webcam_frame.height());
			
			while (true) {
				camera.read(webcam_frame);
				panel1.setimagewithMat(webcam_frame);
				frame1.repaint();
			}
			
		}else {
			System.out.print("Error");
		}
		
	}
	
	
	private static VideoCapture initWebcam() {
		VideoCapture cameraCapture = new VideoCapture();
		cameraCapture.open(0);
		
		return cameraCapture;
	}
	
	

}
