package Ch4;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

public class Ch04_1_1ReadImgAndSave {
	
	static {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}

	public static void main(String[] args) {
		try {
			File input = new File("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\clean.jpg");

			BufferedImage image = ImageIO.read(input);
			BufferedImage imageCopy =new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
			imageCopy.getGraphics().drawImage(image, 0, 0, null);
			byte[] data = ((DataBufferByte) imageCopy.getRaster().getDataBuffer()).getData();  
			Mat source = new Mat(image.getHeight(),image.getWidth(), CvType.CV_8UC3);
			source.put(0, 0, data);     
					
			List<Mat> bgrList = new ArrayList<Mat>(3);
			System.out.println("channels="+source.channels());
			System.out.println("total="+source.total());
			System.out.println("col="+source.cols());
			System.out.println("row="+source.rows());
			System.out.println("size="+source.size());
			System.out.println("depth="+source.depth());
			System.out.println("type="+source.type());
			System.out.println("m="+source.dump());
			
			// (RGB,RGB......9�աA�@9X3=27(col),row=10)
			Core.split(source, bgrList); // split 3 channels ,R(2),G(1),B(0)
			//get only blue channel mat
			System.out.println("blue="+bgrList.get(0).dump());
			//get only green channel mat
			System.out.println("green="+bgrList.get(1).dump());
			//get only red channel mat
			System.out.println("red="+bgrList.get(2).dump());
			
			Imgcodecs.imwrite("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\clean2.jpg",source);
		}catch(Exception ex) {
			ex.printStackTrace();
		}

	}

}
