package Ch4;

import java.awt.EventQueue;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

public class Ch04_1_2SwingReading {

	static {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}
	private JFrame frmjavaSwing;
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				try {
					Ch04_1_2SwingReading window = new Ch04_1_2SwingReading();
					window.frmjavaSwing.setVisible(true);
					
				}catch(Exception ex) {
					ex.printStackTrace();
				}
				
			}
			
		});

	}
	
	public Ch04_1_2SwingReading() {
		initialize();
	}
	
	private void initialize() {
		Mat source = Imgcodecs.imread("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\clean.jpg");
		BufferedImage image = matToBufferedImage(source);
		frmjavaSwing = new JFrame();
		frmjavaSwing.setTitle("Ū���v���� Java Swing ����");
		frmjavaSwing.setBounds(100,100,image.getHeight()+100,image.getWidth()+100);
		frmjavaSwing.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmjavaSwing.getContentPane().setLayout(null);;
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10,10,image.getHeight()+10,image.getWidth()+10);
		lblNewLabel.setIcon(new ImageIcon(image));
		frmjavaSwing.getContentPane().add(lblNewLabel);
		
	}
	
	public BufferedImage matToBufferedImage(Mat matrix) {
		int cols = matrix.cols();
		int rows = matrix.rows();
		
		int elemSize = (int)matrix.elemSize();
		byte[] data = new byte[cols * rows * elemSize]; 
		
		int type;
		matrix.get(0, 0,data);
		switch(matrix.channels()) {
			case 1:
				type = BufferedImage.TYPE_BYTE_GRAY;
			break;
			
			case 3:
			{
				type = BufferedImage.TYPE_3BYTE_BGR;
				// bgr to rgb
				byte b ;
				for(int i = 0 ; i < data.length ; i=i+3) {
					b = data[i];
					data[i] = data[i+2];
					data[i+2] = b;
				}
			}
			break;
			
			default:
				return null;
		}
		BufferedImage image2 = new BufferedImage(cols,rows,type);
		image2.getRaster().setDataElements(0,0,cols,rows,data);
		
		return image2;
	}

}
