package Ch5;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class OpenCV_ToGrayUseCvColor {

	static {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}
	
	public static void main(String[] args) {
		try {
			
			Mat src = Imgcodecs.imread("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\clean.jpg");
			Mat dst = new Mat(src.rows(),src.cols(),src.type());
			Imgproc.cvtColor(src, dst, Imgproc.COLOR_RGB2GRAY);
			Imgcodecs.imwrite("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\clean_gray.jpg",dst);

		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
