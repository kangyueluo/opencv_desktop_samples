package Ch5;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

public class OpenCV_Change_Brightness {
	static double alpha = 2;
	static double beta = 50;

	public static void main(String[] args) {
		try {
			System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
			Mat src = Imgcodecs.imread("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\clean.jpg",Imgcodecs.CV_LOAD_IMAGE_COLOR);
			Mat dstinationMat = new Mat(src.rows(),src.cols(),src.type());
			
			src.convertTo(dstinationMat, -1,alpha,beta);
			
			Imgcodecs.imwrite("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\clean_bright.jpg", dstinationMat);
			
		}catch(Exception e) {
			e.printStackTrace();
		}

	}

}
