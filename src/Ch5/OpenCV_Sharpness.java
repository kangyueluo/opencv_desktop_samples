package Ch5;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class OpenCV_Sharpness {

    static {System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}

    public static void main(String[] args){
        try{
            Mat src = Imgcodecs.imread(
                    "D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\clean.jpg",
                    Imgcodecs.CV_LOAD_IMAGE_COLOR);

            Mat dst = new Mat(src.rows(),src.cols(),src.type());
            Imgproc.medianBlur(src,dst,9);
            Core.addWeighted(src,2.1,dst,-1.1,0,dst);
            Imgcodecs.imwrite("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\clean_sharped.jpg",dst);

        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
