package Ch5;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import java.util.ArrayList;
import java.util.List;

public class OpenCV_Concat {

    public static void main(String[] args){
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        Mat im = Imgcodecs.imread("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\ncku.jpg");
        List<Mat> matList=new ArrayList<Mat>();
        Mat hdst=new Mat();
        Mat vdst=new Mat();
        //3¦¸
        matList.add(im);
        matList.add(im);
        matList.add(im);
        Core.hconcat(matList, hdst);
        Core.vconcat(matList, vdst);

        Imgcodecs.imwrite("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\hconcat_ncku.jpg",hdst);
        Imgcodecs.imwrite("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\vconcat_ncku.jpg",vdst);
    }
}
