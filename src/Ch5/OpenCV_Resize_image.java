package Ch5;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

public class OpenCV_Resize_image {

    static {System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}

    private JFrame frmjavaSwing;
    private Mat src = new Mat();

    /**
     *  Launcher the application
     */
    public static void main(String[] args){
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try{
                    OpenCV_Resize_image window = new OpenCV_Resize_image();
                    window.frmjavaSwing.setVisible(true);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     *  Imitialize the content of the frame
     */
    public OpenCV_Resize_image(){
        init();
    }

    /**
     *  Init the contents of the frame.
     */
    private void init() {
        final Mat src = Imgcodecs.imread(
                "D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\clean.jpg");
        setSrc(src);
        BufferedImage image=matToBufferedImage(src);

        frmjavaSwing = new JFrame();
        frmjavaSwing.setTitle("opencv ��j�Y�p�m��4,�ϥ�resize�B��Ҥ��P");
        frmjavaSwing.setBounds(100, 100, 520, 550);
        frmjavaSwing.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmjavaSwing.getContentPane().setLayout(null);

        final JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setBounds(10, 68, 438, 438);
        lblNewLabel.setIcon(new ImageIcon(image));
        frmjavaSwing.getContentPane().add(lblNewLabel);

        JButton btnNewButton = new JButton("��j2��");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                BufferedImage newImage=matToBufferedImage( resize(getSrc(),2));
                lblNewLabel.setIcon(new ImageIcon(newImage));
            }
        });
        btnNewButton.setBounds(63, 21, 87, 23);
        frmjavaSwing.getContentPane().add(btnNewButton);

        JButton button = new JButton("�Y�p2��");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                BufferedImage newImage=matToBufferedImage( resize(getSrc(),0.5));
                lblNewLabel.setIcon(new ImageIcon(newImage));
            }
        });
        button.setBounds(221, 21, 87, 23);
        frmjavaSwing.getContentPane().add(button);
    }

    public Mat resize(Mat source,double size){

        Mat destination=new Mat(source.rows()*(int)size,source.cols()*(int)size*2,source.type());
        Imgproc.resize(source, destination,destination.size(), size,size*2,0);
        setSrc(destination);
        return destination;

    }

    public BufferedImage matToBufferedImage(Mat matrix) {
        int cols = matrix.cols();
        int rows = matrix.rows();
        int elemSize = (int)matrix.elemSize();
        byte[] data = new byte[cols * rows * elemSize];
        int type;
        matrix.get(0, 0, data);
        switch (matrix.channels()) {
            case 1:
                type = BufferedImage.TYPE_BYTE_GRAY;
                break;
            case 3:
                type = BufferedImage.TYPE_3BYTE_BGR;
                // bgr to rgb
                byte b;
                for(int i=0; i<data.length; i=i+3) {
                    b = data[i];
                    data[i] = data[i+2];
                    data[i+2] = b;
                }
                break;
            default:
                return null;
        }
        BufferedImage image2 = new BufferedImage(cols, rows, type);
        image2.getRaster().setDataElements(0, 0, cols, rows, data);
        return image2;
    }

    public Mat getSrc() {
        return this.src;
    }

    public void setSrc(Mat source) {
        this.src = source;
    }
}
