package Ch5;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;

public class OpenCV_Convert_Use_Bitwise_Xor {

    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    public static void main(String[] args) {
        try{
            Mat src = Imgcodecs.imread("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\clean.jpg");
            Mat destination = new Mat(src.rows(),src.cols(),src.type(),new Scalar(255,255,255));
            Mat dst = new Mat(src.rows(),src.cols(),src.type());

            Core.bitwise_xor(src,destination,dst);
            Imgcodecs.imwrite("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\clean_xor.jpg",dst);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
