package Ch5;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

public class OpenCV_Mosaic {

    public static void main(String[] args){
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        Mat im  = Imgcodecs.imread("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\clean.jpg");

        Imgproc.resize(im,im,new Size(),0.1,0.1,Imgproc.INTER_NEAREST);
        Imgproc.resize(im,im,new Size(),10.0,10.0,Imgproc.INTER_NEAREST);
        Imgcodecs.imwrite("D:\\projects\\Java\\OpenCV_Samples\\resource\\imgs\\clean_mosaic.jpg", im);
    }
}
