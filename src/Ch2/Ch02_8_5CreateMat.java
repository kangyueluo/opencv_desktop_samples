package Ch2;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class Ch02_8_5CreateMat {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		Mat m1 = new Mat(2,2,CvType.CV_8UC1) {
			{
				put(0,0,1);
				put(0,1,2);
				put(1,0,3);
				put(1,1,4);
			}
		};
		
		Mat m2 = Mat.zeros(1,3,CvType.CV_8UC1);
		System.out.println("�x�}m2�Ҧ�����:"+m2.dump());
	}

}
