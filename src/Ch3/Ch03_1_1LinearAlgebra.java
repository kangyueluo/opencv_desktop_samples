package Ch3;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class Ch03_1_1LinearAlgebra {

	public static void main(String[] args) {
		// TODO Auto-generated method stub		
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		Mat m1 = new Mat(2,2,CvType.CV_32FC1);
		m1.put(0, 0, 1);
		m1.put(0, 1, 2);
		m1.put(1, 0, 3);
		m1.put(1, 1, 4);
		
		Mat m2 = new Mat(2,2,CvType.CV_8UC1);
		System.out.println("�x�}m1�Ҧ�����:"+m1.dump());
		System.out.println("m1��m�x�}:"+m1.t().dump());
		
		Mat transpose = new Mat();
		Core.transpose(m1, transpose);
		System.out.println("m1��m�x�}�t�~�@�ؼg�k:"+transpose.dump());
		
		System.out.println("m1�ϯx�}:"+m1.inv().dump());
		Mat invert = new Mat();
		Core.invert(m1, invert);
		System.out.println("m1�ϯx�}�t�~�@�ؼg�k:"+invert.dump());
		System.out.println("m1���x�}:"+m2.dump());
		
		Mat m4 = m1.reshape(4,1);
		System.out.println("�x�}m4�Om1�૬��1�C4��x�}�A�ҥѤ���:"+m4.dump());
		
		Mat m5 = m1.reshape(1,4);
		System.out.println("�x�}m5�Om1�૬��4�C1��x�}�A�ҥѤ���:"+m5.dump());
		
		Mat m6 = new Mat();
		m6.push_back(m1);
		m6.push_back(m1);
		System.out.println("m6 �Ҧ�����:"+m6.dump());
		
	}

}
