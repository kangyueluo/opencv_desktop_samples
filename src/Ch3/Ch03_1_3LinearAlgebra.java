package Ch3;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;

public class Ch03_1_3LinearAlgebra {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Mat m1 = new Mat(2,2,CvType.CV_32FC1);
		m1.put(0, 0, 1);
		m1.put(0, 1, 2);
		m1.put(1, 0, 3);
		m1.put(1, 1, 4);
	
		Mat m2 = m1.clone();
		System.out.println("�x�}m2�O�ƻsm1�A�Ҧ�����="+m2.dump());
		
		Mat m3 = new Mat();
		System.out.println("�x�}m2��trace(��)="+Core.trace(m2).val[0]);
		
		// �D�X eigenvalues eigenvectors
		Mat eigenvalues = new Mat();
		Mat eigenvectors = new Mat();
		boolean computeEigenvectors = true;
		Core.eigen(m1, eigenvalues,eigenvectors);
		System.out.println("m1 �S�x��"+eigenvalues.dump());
		System.out.println("m1 �S�x�V�q"+eigenvectors.dump());
		
		// ���t /�@�ܼƯx�} (covariance matrix)
		Mat covar = new Mat(2,2,CvType.CV_32FC1);
		Mat mean = new Mat(1,2,CvType.CV_32FC1);
		Core.calcCovarMatrix(m2, covar, mean, Core.COVAR_ROWS|Core.COVAR_NORMAL|CvType.CV_32F);
		System.out.println("m2���t/�@�ܼƯx�}="+covar.dump()+"by col����"+mean.dump());
		
		//�ǭǤ��
		Mat compare = new Mat();
		Mat m4 = new Mat(2,2,CvType.CV_32FC1,new Scalar(9));
		Core.compare(m1, m4, compare, Core.CMP_GT); // �O�_�j��
		System.out.println("m1�O�_�j��m4�U����"+compare.dump());
		compare = new Mat();
		Core.compare(m1, m4, compare, Core.CMP_LT); // �O�_�p��
		System.out.println("m1�O�_�p��m4�U����"+compare.dump());
		compare = new Mat();
		Core.compare(m1, m4, compare, Core.CMP_EQ); // �O�_����
		System.out.println("m1�O�_����m4�U����"+compare.dump());
		compare = new Mat();
		Core.compare(m1, m4, compare, Core.CMP_GT); // �O�_�j�󵥩�
		System.out.println("m1�O�_�j�󵥩�m4�U����"+compare.dump());
		compare = new Mat();
		Core.compare(m1, m4, compare, Core.CMP_LT); // �O�_�p�󵥩�
		System.out.println("m1�O�_�p�󵥩�m4�U����"+compare.dump());
		
		
	}

}
