package Ch1;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

public class Gui_Slider {
    private JFrame frame;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Gui_Slider window = new Gui_Slider();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public Gui_Slider() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 450, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JLabel lblNewLabel = new JLabel("Test");
        lblNewLabel.setBounds(14, 20, 57, 19);
        frame.getContentPane().add(lblNewLabel);

        JLabel lblNewLabel1 = new JLabel("");
        lblNewLabel1.setBounds(307, 20, 57, 19);
        frame.getContentPane().add(lblNewLabel1);

        JSlider slider = new JSlider();
        slider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                lblNewLabel1.setText(slider.getValue()+"");
            }
        });
        slider.setValue(1);
        slider.setMinimum(1);
        slider.setBounds(93, 13, 200, 26);
        frame.getContentPane().add(slider);
    }
}